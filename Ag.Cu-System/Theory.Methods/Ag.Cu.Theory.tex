
%%% Chapter heading commands %%%

%%% Path to the directorz containing the graphics and figures 
%\graphicspath{{./png/}} 

\chapter{Contour Detection Based on non-CRF Inhibition}
\label{chapter:contour_detection}

\renewcommand{\publ}{\flushleft\footnotesize{Published as:
C. Grigorescu, N. Petkov and M.A. Westenberg -- \textit{``Contour
Detection Based on non-CRF Inhibition,''} IEEE Transactions on Image
Processing, vol. 12, no. 7, pp. 729--739, July 2003.}}

\epigraph{The question is not what you look at, but what you
see.}{Henry David Thoreau}
\index{Contour detection techniques!non-CRF inhibition}

%%% Abstract %%%

\begin{Abstract}
We propose a biologically motivated computational step, called
non-classical receptive field (non-CRF) inhibition, more generally
surround inhibition or suppression, to improve contour detection in
machine vision. Non-CRF inhibition is exhibited by 80\% of the
orientation selective neurons in the primary visual cortex of monkeys
and has been demonstrated to influence the visual perception of man as
well. The essence of this mechanism is that the response of an edge
detector in a certain point is suppressed by the responses of the
operator in the region outside the area of operator support. We
combine classical edge detection with two types of inhibitory
mechanism, isotropic and anisotropic inhibition, both of which have
counterparts in biology. For edge detection, we also use a
biologically motivated method (the Gabor energy operator). The
resulting operator responds strongly to isolated lines, edges, and
contours, but exhibits a weaker or no response to edges that make part
of texture.

\end{Abstract}

%%% Chapter sections %%%

\index{Visual system}
\section{Introduction}
\label{sec:biological_aspects}
\PARstart{O}ne of the most frequently encountered tasks in machine
vision is to find the contours of objects in a complex scene. Yet,
state-of-the-art edge detection techniques do not differentiate
between object contours --- these are the actual primitives needed in
most applications --- and edges originating from textured
regions. There is evidence that the human visual system makes such a
difference in its early stages of visual information processing, and
that isolated edges, on one hand, and edges in a group, on the other
hand, are perceived in different ways.  In this chapter we propose two
biologically motivated models for contour detection and show their
applicability in contour detection in natural images.

\bigskip

An important finding in the neurophysiology of the visual system of
monkeys and cats, made in the beginning of the 1960s --- i.e. before
the development of edge detection algorithms for digital image
processing --- was that the majority of neurons in the primary visual
cortex respond to a line or an edge of a certain orientation in a
given position of the visual field. Initially, two types of
orientation selective neuron were found, one that was sensitive to the
contrast polarity of lines and edges, called simple cell, and another
that was not, called complex cell \cite{Hubel62,Hubel74}. 
\index{Complex cell!response}

\index{Gabor functions!used in applications} These computational
models gave the basis for biologically motivated edge detection
algorithms in image processing. In particular, a family of two
dimensional Gabor functions was proposed as a model of the linear
filtering properties of simple cells
\cite{Daugman80,Daugman85}. 

%%% Example figure %%%

\begin{figure}[tpb]
  \begin{center} \includegraphics[width=0.45\textwidth]{./chapter_3/png/tobin-blakemore.png}
    \caption{The effect of orientation contrast in non-CRF inhibition: the
    plot shows the response of a neuron to a stimulus composed of a single bar
    of optimal orientation in the CRF (central circle) and a grating of
    varying orientation outside the CRF. The inhibition by the surrounding
    grating is strongest when its orientation coincides with the optimal
    stimulus. (Courtesy of C. Blakemore and Exp. Brain Res.).}
    \label{fig:Blakemore} \end{center}
\end{figure}

%%% Example table %%%

\index{Performance!contour detection}
\begin{table}[htpb]
  \begin{center}
    \footnotesize
    \begin{tabular}{llcccccc}
\hline
\textbf{Goat 3} 
      & Gabor energy & $2.0$ & $0.1$ &       & $0.72$ & $0.38$ & $0.25$ \\
      & Canny        & $2.4$ & $0.1$ &       & $0.83$ & $0.55$ & $0.14$ \\
      & Anisotropic  & $2.4$ & $0.1$ & $1.2$ & $0.36$ & $0.60$ & $0.32$ \\
      & Isotropic    & $2.0$ & $0.1$ & $1.0$ & $0.46$ & $0.51$ & $0.34$\\ \hline
\textbf{Elephant 2} 
      & Gabor energy & $2.0$ & $0.1$ &       & $0.59$ & $0.36$ & $0.32$ \\
      & Canny        & $2.4$ & $0.1$ &       & $0.71$ & $0.50$ & $0.23$ \\
      & Anisotropic  & $2.4$ & $0.1$ & $1.2$ & $0.36$ & $0.45$ & $0.40$ \\
      & Isotropic    & $2.0$ & $0.1$ & $1.0$ & $0.31$ & $0.49$ & $0.42$\\ \hline
\textbf{Hyena}  
      & Gabor energy & $2.0$ & $0.1$ &       & $0.52$ & $0.32$ & $0.39$ \\
      & Canny        & $2.2$ & $0.1$ &       & $0.59$ & $0.50$ & $0.28$ \\
      & Anisotropic  & $2.4$ & $0.1$ & $1.2$ & $0.37$ & $0.25$ & $0.51$ \\
      & Isotropic    & $2.0$ & $0.1$ & $1.0$ & $0.22$ & $0.35$ & $0.55$\\ \hline
\textbf{Gazelle 2} 
      & Gabor energy & $2.0$ & $0.1$ &       & $0.61$ & $0.48$ & $0.32$ \\
      & Canny        & $1.6$ & $0.2$ &       & $0.72$ & $0.38$ & $0.23$ \\
      & Anisotropic  & $1.6$ & $0.2$ & $1.0$ & $0.51$ & $0.42$ & $0.36$ \\
      & Isotropic    & $1.6$ & $0.2$ & $1.0$ & $0.44$ & $0.46$ & $0.38$ \\
    \end{tabular}
  \caption{Operator parameters, errors, and performances for the images
    presented in Fig.~\protect\ref{Fig:Images}.}
  \label{tab:performance}
  \end{center}
\end{table}


