\begin{tikzpicture}

\pgftransformscale{.6}
\pgftransformrotate{-90}

%Legend

\draw (10.3,6.2) node {Surface Pd:};
\shade[ball color=green, fill opacity = .5] (10.3, 8.4 )   circle(6mm);

\draw(10.3,11.8) node {Oxygen:};
\shade[ball color=red, fill opacity = .5] (10.3, 13.4)circle(4mm); 

\draw (12.0,6.2) node {Pd(100) hollow:};
\shade[ball color=black!20] (12.0 , 8.4) circle(2mm); 

\draw (12.0,11.2) node {Pd(100) bridge:};
\draw (12.0,13.4) node[fill=black!20]{};

\draw(1,5)rectangle(9.5,14);
\clip(1,5)rectangle(9.5,14);

\pgftransformshift{\pgfpoint{1.5cm}{-1cm}}

\foreach \x in {-2,...,2}{
  \foreach \y in {-2,...,2}{
    %***Definition of coordinates***
    %Pd(100) hollow sites
    \node(h1) at (6.296+\x*6.296+0.34408790508233,6.296+\y*6.296+0.926618296945){};
    \node(h2) at (\x*6.296+ 1.59113049268837, \y*6.296+ 3.43780332375807) {};
    \node(h3) at (\x*6.296+ 2.84574797953539, \y*6.296+ 5.96310098461172) {}; 
    \node(h4) at (\x*6.296+ 4.10464369172027, 6.296+\y*6.296+2.21435558239077) {};
    \node(h5) at (\x*6.296+ 5.31053190835417, \y*6.296+ 4.75473569641011) {};


    %Oxide Pd
    \node(Pd1) at (\x*6.296+3.2310128485632, \y*6.296+6.2328684774094 ){}  ; 
    \node(Pd2) at (\x*6.296+0.0867943321959671,\y*6.296+6.25895314440737){};
    \node(Pd3) at (\x*6.296+1.65525977156214, \y*6.296+3.23429832279509){} ; 
    \node(Pd4) at (\x*6.296+4.78666152813302, \y*6.296+3.18465154488858){} ; 

    \node(Pd3p10) at (6.296+\x*6.296+1.65525977156214, \y*6.296+3.23429832279509){} ; 


    \node(Pd2p01) at (\x*6.296+0.0867943321959671,6.296+\y*6.296+6.25895314440737){};
    \node(Pd2p10) at (6.296+\x*6.296+0.0867943321959671,\y*6.296+6.25895314440737){};
    
    %Oxide Oxygen
    \node(O1) at (\x*6.296+1.64973919642545, \y*6.296+5.06314831724067){} ;
    \node(O2) at (\x*6.296+4.81706394091531, \y*6.296+5.04027686526835){} ;
    \node(O3) at (\x*6.296+1.67703227863746, \y*6.296+1.22762720073791){} ;
    \node(O4) at (\x*6.296+4.78808503194601, \y*6.296+1.1931186262743 ){} ;    
    
    %"Periodic boundary condition" points
    \node(Pd2p01) at (Pd2)[right=6.296cm]{};
    \node(Pd2p10) at (Pd2)[below=6.296cm]{};
    \node(h1p01) at (h1)[right=6.296cm]{};
    \node(h1p10) at (h1)[below=6.296cm]{};
    \node(h2p01) at (\x*6.296+ 1.59113049268837, 6.296+\y*6.296+ 3.43780332375807 ) {};
    \node(h2p10) at (6.292+\x*6.296+ 1.59113049268837, 0*6.296+\y*6.296+ 3.43780332375807 ) {};
    \node(h2p12) at (2*6.296+\x*6.296+ 1.59113049268837, -6.296+\y*6.296+ 3.43780332375807 ) {};
    \node(h2p11) at (1*6.296+\x*6.296+ 1.59113049268837, 6.296+\y*6.296+ 3.43780332375807 ) {};
    \node(h5p01) at (\x*6.296+ 5.31053190835417, 6.296+\y*6.296+ 4.75473569641011 ) {};
    \node(h3p10) at (6.296+\x*6.296+ 2.84574797953539, \y*6.296+ 5.96310098461172 ) {}; 
        
    
    %***Visible Objects***

    %-1 substrate layer, i.e. Pd(100) hollow sites
    \foreach \i in {1,...,5}{
      \shade[ball color=black!20] (h\i) circle(2mm); 
    }
    %Pd(100) Bridge positions
    \foreach \from/\to in {h2/h3,h3/h5,h3/h4,h4/h1,h5/h1,h1/h2p11,h4/h5p01,h1/h3p10,h2p01/h4,h2p10/h5}{
      \draw[draw=none] (\from) --(\to) node[midway, fill=black!20]{};
      %Debug label
        %\draw[draw=none] (\from) --(\to) node[midway, fill=black!20]{\from-\to};
    }
    \begin{scope}[fill opacity = .5]
    \foreach \i in {1,...,4}{
        %surface Pd
        \shade[ball color=green] (Pd\i) circle(6mm);
    }
    \foreach \i in {1,...,4}{
      %oxygen
        \shade[ball color=red] (O\i) circle (4mm); 
    }
    \end{scope}
    %Oxygen labels
      \draw(O1) node {top-B};
      \draw(O2) node {top-A};
      \draw(O3) node {bottom-B};
      \draw(O4) node {bottom-A};

    %p.u.c. boundaries
    \draw [dotted] (h1)--(h1p01);
    \draw [dotted] (h1)--(h1p10);
    %%Debug labels
    \foreach \i in {1,...,5}{
      %\draw(h\i) node {h\i};
    }
    \foreach \i in {1,...,4}{
      %\draw(O\i) node {O\i};
      %\draw(O\i) node {O};
      %\draw(Pd\i) node {Pd\i};
    }
  }
}
\end{tikzpicture}
