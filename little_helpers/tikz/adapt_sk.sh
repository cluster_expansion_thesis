#!/bin/bash -eu


#Default scaling
scaling=".3"

#Parse options
while getopts "s:" optionName
do
  case "${optionName}" in
    s) scaling="${OPTARG}" ;;
  esac
done

#Switch to not processed arguments
shift $(($OPTIND - 1))

filename=${1}
tmpfile=$(mktemp)

cp ${filename} ${tmpfile}

cat ${tmpfile}  \
  | sed -e "s/\(usepackage{tikz}\)/\1\n\\\\usepackage[graphics,tightpage,active]{preview}\n\n\\\\PreviewEnvironment{tikzpicture} \n/" \
  | sed -e "s/\(begin{tikzpicture}\[join=round\]\)/\1\n\\\\pgftransformscale{${scaling}}\n/" \
  | sed -e "s/\(begin{tikzpicture}\[join=round\]\)/\1\n\\\\definecolor{substrate_blue}{rgb}{0,.02,.6}/" \
  | sed -e "s/\(begin{tikzpicture}\[join=round\]\)/\1\n\\\\definecolor{oxide_green}{rgb}{.92,1.,.90}/" \
  | sed -e "s/\(begin{tikzpicture}\[join=round\]\)/\1\n\\\\definecolor{oxygen}{rgb}{1.,.34,.28}/" \
  > ${filename}

rm ${tmpfile}

