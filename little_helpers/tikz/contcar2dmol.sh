#!/bin/bash -eu
#------------------------------------------------
#    contcar2dmol.sh
#    Hackish script to convert VASP structure file with periodic
#    boundary conditions and orthogonal lattice into dmol file
#    Copyright (C) Max J. Hoffmann 2009 mjhoffmann@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Based on documentation found at 
#  http://cms.mpi.univie.ac.at/vasp/vasp.pdf.gz , subsection 5.7 POSCAR file
#------------------------------------------------

#Initialize variables
contcar=""

for i in $(seq 7) 
do
  species_name[${i}]=""
done

#Parse options
while getopts "hi:a:b:c:d:e:f:g:" optionName
#Sorry, about the clumsy option names, but getopts does
#not handle long options
do
  case "${optionName}" in
    a) species_name[1]=${OPTARG} ;;
    b) species_name[2]=${OPTARG} ;;
    c) species_name[3]=${OPTARG} ;;
    d) species_name[4]=${OPTARG} ;;
    e) species_name[5]=${OPTARG} ;;
    f) species_name[6]=${OPTARG} ;;
    g) species_name[7]=${OPTARG} ;;
    h) echo "Usage: ${0} -i CONTCAR_FILE [-a Species1 -b Species2 ...] > DMOL_FILE " ;
       echo "Only 7 species can be specified due to limitations of getopts"
       exit ;;
    i) contcar=${OPTARG} ;;
  esac
done


#Exit with usage message if no input file is provided
if [ "${contcar}" = "" ]
then
   ${0} -h
   exit
fi

#INPUT
#fetch maximum number of species
max_spec=$(awk '{if(NR==6) print $0}' ${contcar} | wc -w)


#global scaling factor
a=$(awk '{if(NR==2) print $1}' ${contcar})
#first lattice vector
x1=$(awk '{if(NR==3) print $1}' ${contcar})
x2=$(awk '{if(NR==3) print $2}' ${contcar})
x3=$(awk '{if(NR==3) print $3}' ${contcar})

#second lattice vector
y1=$(awk '{if(NR==4) print $1}' ${contcar})
y2=$(awk '{if(NR==4) print $2}' ${contcar})
y3=$(awk '{if(NR==4) print $3}' ${contcar})

#third lattice vector
y1=$(awk '{if(NR==4) print $1}' ${contcar})
z1=$(awk '{if(NR==5) print $1}' ${contcar})
z2=$(awk '{if(NR==5) print $2}' ${contcar})
z3=$(awk '{if(NR==5) print $3}' ${contcar})

#fetch numbers of species
for i in $(seq ${max_spec})
do
  sp[${i}]=$(awk "{if(NR==6) print \$${i}}" ${contcar})
done


#OUTPUT
#lattice constants
echo "\$cell vectors"
head -n 5 ${contcar} | tail -n 3
echo "\$coordinates"

#atomic coordinates
accum=0
species_nr=1
for atoms in ${sp[@]}
do

  #The seventh line may contain [sS]elective Dynamics and is optional, so check ...
  #I did not understand the meaning of this line. So the results might be wrong if there is
  #no 'selective dynamics' printed.
  if [  -n "$(sed -n 7p ${contcar} | grep -i "^\s*s")" ]; then offset=8; else offset=7; fi
  for i in $(seq ${atoms})
  do
    line=$(expr "${offset}+${accum}+${i}" | bc -l)

    #Switch between direct (fractional) coordinates and cartesian(absolute) coordinates
    if [ $(sed -n ${offset}p ${contcar} | grep -oi "^\s*d" )  ]
    then
      abs_x=$(expr "${a}*($(awk "{if(NR==${line}) print \$1 }" ${contcar})*${x1}+\
      $(awk "{if(NR==${line}) print \$2 }" ${contcar})*${y1}+\
      $(awk "{if(NR==${line}) print \$3 }" ${contcar})*${z1})" | bc -l )
      abs_y=$(expr "${a}*($(awk "{if(NR==${line}) print \$1 }" ${contcar})*${x2}+\
      $(awk "{if(NR==${line}) print \$2 }" ${contcar})*${y2}+\
      $(awk "{if(NR==${line}) print \$3 }" ${contcar})*${z2})" | bc -l )
      abs_z=$(expr "${a}*($(awk "{if(NR==${line}) print \$1 }" ${contcar})*${x3}+\
      $(awk "{if(NR==${line}) print \$2 }" ${contcar})*${y3}+\
      $(awk "{if(NR==${line}) print \$3 }" ${contcar})*${z3})" | bc -l )
    else
      abs_x=$(awk "{if(NR==${line}) print \$1 }" ${contcar})
      abs_y=$(awk "{if(NR==${line}) print \$2 }" ${contcar})
      abs_z=$(awk "{if(NR==${line}) print \$3 }" ${contcar})
    fi
    if [ "${species_name[${species_nr}]}" = "" ] 
    then
      echo "Species${species_nr} ${abs_x} ${abs_y} ${abs_z}"
    else
      echo "${species_name[${species_nr}]} ${abs_x} ${abs_y} ${abs_z}"
    fi

  done
  accum=$(expr "${accum}+${atoms}" | bc -l)
  species_nr=$(expr "${species_nr} + 1" | bc -l)

done

#And we are done!
echo "\$end"
