#!/bin/bash -eu
#------------------------------------------------
#    contcar2sketch.sh
#    Wrapper script that pipes contcar -> dmol -> sketch
#    used in a sketch input file.
#    Sketch lives at http://www.frontiernet.net/~eugene.ressler/
#
#    Copyright (C) Max J. Hoffmann 2009 mjhoffmann@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------


contcar=""
while getopts "i:ha:b:c:d:e:f:g:" optionName
do
  case "${optionName}" in 
    h) echo "Usage: ${0} -i CONTCAR_FILE > SKETCH_FILE" ;
       exit;;
    i) contcar=${OPTARG} ;;
  esac
done

#Exit with usage message if no input file is provided
if [ "${contcar}" = "" ]
then
   ${0} -h
   exit
fi

dmol_temp=$(mktemp)

contcar2dmol.sh $@ > ${dmol_temp}
dmol2sketch.sh -i ${dmol_temp}

rm -f ${dmol_temp}
