#!/bin/bash -eu
#------------------------------------------------
#    dmol2sketch.sh
#    Helper script that turns dmol file into points in syntax that can be
#    used in a sketch input file.
#    Sketch lives at http://www.frontiernet.net/~eugene.ressler/
#
#    Copyright (C) Max J. Hoffmann 2009 mjhoffmann@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------

#Initialize variables
dmol=""


while getopts "i:h" optionName
do
  case "${optionName}" in 
    h) echo "Usage: ${0} -i DMOL_FILE > SKETCH_FILE" ;
       exit;;
    i) dmol=${OPTARG} ;;
  esac
done


#Exit with usage message if no input file is provided
if [ "${dmol}" = "" ]
then
   ${0} -h
   exit
fi

#Find lines where atomic coordinates are stored
start_line=$(expr "$(grep -rin coordinates ${dmol} | cut -d ":" -f 1) + 1" | bc -l)
end_line=$(expr "$(wc -l ${dmol} | awk '{print $1}' ) -1 " | bc -l)

#Determine starting species
species=$(sed -n ${start_line}p ${dmol} | awk '{print $1}')
#Initialize atom counter
atom_nr=0

for line in $(seq ${start_line} ${end_line})
do
  if [  "$(sed -n ${line}p ${dmol} | awk '{print $1}')" = "${species}" ]
  then
    atom_nr="$(expr "${atom_nr}+1" | bc -l)"
  else 
    atom_nr=1
    species="$(sed -n ${line}p ${dmol} | awk '{print $1}')"
  fi


  sed -n ${line}p ${dmol}| sed -e "s/\([^ ]*\)\s*\([0-9.-]*\)\s*\([0-9.-]*\)\s*\([0-9.-]*\)/def \1${atom_nr} (\2,\3,\4)/"
done

