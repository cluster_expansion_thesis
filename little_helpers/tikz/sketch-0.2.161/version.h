// Produced by makever.pl.  Don't edit.
#define VER_MAJOR 0
#define VER_MINOR 2
#define VER_BUILD 161
#define VER_BUILD_TIME 1252467327
#ifndef STRINGIFY
#define ___S(X) #X
#define STRINGIFY(X) ___S(X)
#endif
#define VER_BUILD_TIME_STRING STRINGIFY(Tue Sep  8 23:35:27 2009)
#define VER_STRING STRINGIFY(VER_MAJOR) "." STRINGIFY(VER_MINOR) " (build " STRINGIFY(VER_BUILD) ", " VER_BUILD_TIME_STRING ")"
