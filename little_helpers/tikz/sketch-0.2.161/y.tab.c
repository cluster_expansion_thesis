/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     ID = 258,
     PAREN_ID = 259,
     BRACKET_ID = 260,
     DBL_BRACKET_ID = 261,
     CURLY_ID = 262,
     ANGLE_ID = 263,
     NUM = 264,
     OPTS_STR = 265,
     SPECIAL = 266,
     TICK = 267,
     THEN = 268,
     DEF = 269,
     EMPTY_ANGLE = 270,
     DOTS = 271,
     LINE = 272,
     CURVE = 273,
     POLYGON = 274,
     REPEAT = 275,
     SWEEP = 276,
     PUT = 277,
     TRANSLATE = 278,
     ROTATE = 279,
     SCALE = 280,
     PROJECT = 281,
     PERSPECTIVE = 282,
     VIEW = 283,
     SQRT = 284,
     SIN = 285,
     COS = 286,
     ATAN2 = 287,
     UNIT = 288,
     INVERSE = 289,
     GLOBAL = 290,
     SET = 291,
     PICTUREBOX = 292,
     FRAME = 293,
     CAMERA = 294,
     LANGUAGE = 295,
     PSTRICKS = 296,
     TIKZ = 297,
     LaTeX = 298,
     ConTeXt = 299,
     NEG = 300
   };
#endif
/* Tokens.  */
#define ID 258
#define PAREN_ID 259
#define BRACKET_ID 260
#define DBL_BRACKET_ID 261
#define CURLY_ID 262
#define ANGLE_ID 263
#define NUM 264
#define OPTS_STR 265
#define SPECIAL 266
#define TICK 267
#define THEN 268
#define DEF 269
#define EMPTY_ANGLE 270
#define DOTS 271
#define LINE 272
#define CURVE 273
#define POLYGON 274
#define REPEAT 275
#define SWEEP 276
#define PUT 277
#define TRANSLATE 278
#define ROTATE 279
#define SCALE 280
#define PROJECT 281
#define PERSPECTIVE 282
#define VIEW 283
#define SQRT 284
#define SIN 285
#define COS 286
#define ATAN2 287
#define UNIT 288
#define INVERSE 289
#define GLOBAL 290
#define SET 291
#define PICTUREBOX 292
#define FRAME 293
#define CAMERA 294
#define LANGUAGE 295
#define PSTRICKS 296
#define TIKZ 297
#define LaTeX 298
#define ConTeXt 299
#define NEG 300




/* Copy the first part of user declarations.  */
#line 21 "sketch.y"


#include <stdio.h>
#include <stdlib.h>

#if defined(_WIN32)
#include <malloc.h>
#if !defined(alloca)
#define alloca _alloca
#endif
#define YYSTACK_USE_ALLOCA 1
// turn of warning about unused goto label in bison skeleton
#pragma warning(disable:4102)
#endif

#include "parse.h"
#include "expr.h"
#include "bsp.h"
#include "global.h"

int yylex(void);

void yyerror (char *s)  /* Called by yyparse on error */
{
  extern SRC_LINE line;
  err(line, "%s", s);
}

static SYMBOL_TABLE *sym_tab;
static BSP_TREE bsp;
static FILE *yyout;

// exported parse tree and global environment
static OBJECT *objects;



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 58 "sketch.y"
typedef union YYSTYPE {
  char *str;
  FLOAT flt;
  POINT_3D pt;
  VECTOR_3D vec;
  TRANSFORM xf;
  EXPR_VAL exv;
  SYMBOL_NAME name;
  OBJECT *obj;
  OPTS *opts;
  int bool;
  int index;
} YYSTYPE;
/* Line 196 of yacc.c.  */
#line 226 "y.tab.c"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 219 of yacc.c.  */
#line 238 "y.tab.c"

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T) && (defined (__STDC__) || defined (__cplusplus))
# include <stddef.h> /* INFRINGES ON USER NAME SPACE */
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if defined (__STDC__) || defined (__cplusplus)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     define YYINCLUDED_STDLIB_H
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2005 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM ((YYSIZE_T) -1)
#  endif
#  ifdef __cplusplus
extern "C" {
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if (! defined (malloc) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if (! defined (free) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifdef __cplusplus
}
#  endif
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE))			\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  32
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   589

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  60
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  35
/* YYNRULES -- Number of rules. */
#define YYNRULES  110
/* YYNRULES -- Number of states. */
#define YYNSTATES  254

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   300

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      58,    59,    47,    46,    56,    45,    49,    48,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    54,     2,    55,    51,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    52,    57,    53,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      50
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short int yyprhs[] =
{
       0,     0,     3,     6,    11,    12,    15,    17,    20,    25,
      30,    33,    35,    38,    41,    43,    46,    48,    50,    53,
      54,    56,    58,    62,    63,    65,    68,    70,    72,    74,
      78,    82,    86,    91,    95,    97,    99,   101,   105,   109,
     113,   117,   127,   137,   145,   151,   155,   158,   160,   161,
     162,   168,   170,   171,   175,   177,   179,   181,   185,   186,
     188,   191,   193,   195,   199,   201,   203,   205,   207,   209,
     213,   217,   221,   225,   229,   233,   237,   240,   244,   248,
     252,   256,   260,   264,   270,   273,   275,   277,   279,   287,
     293,   295,   297,   305,   311,   313,   315,   354,   358,   364,
     372,   376,   380,   383,   387,   391,   399,   405,   409,   413,
     415
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const yysigned_char yyrhs[] =
{
      61,     0,    -1,    70,    62,    -1,    35,    52,    63,    53,
      -1,    -1,    63,    64,    -1,    64,    -1,    36,    10,    -1,
      37,    54,    88,    55,    -1,    37,    69,    89,    89,    -1,
      39,    94,    -1,    38,    -1,    38,    10,    -1,    40,    65,
      -1,    73,    -1,    66,    67,    -1,    41,    -1,    42,    -1,
      56,    68,    -1,    -1,    43,    -1,    44,    -1,    54,    88,
      55,    -1,    -1,    71,    -1,    71,    72,    -1,    72,    -1,
      73,    -1,    76,    -1,    14,     3,    75,    -1,    74,    15,
      75,    -1,    14,     3,    15,    -1,    14,     3,     8,    75,
      -1,    74,     8,    75,    -1,    86,    -1,    76,    -1,    10,
      -1,    16,    81,    82,    -1,    17,    81,    82,    -1,    18,
      81,    82,    -1,    19,    81,    82,    -1,    21,    81,    52,
      88,    79,    56,    84,    53,    89,    -1,    21,    81,    52,
      88,    79,    56,    84,    53,    76,    -1,    20,    52,    88,
      56,    84,    53,    76,    -1,    22,    52,    94,    53,    76,
      -1,    11,    81,    82,    -1,    11,    81,    -1,     7,    -1,
      -1,    -1,    52,    77,    70,    78,    53,    -1,    15,    -1,
      -1,    80,    56,     3,    -1,     3,    -1,    10,    -1,     5,
      -1,    54,    80,    55,    -1,    -1,    83,    -1,    83,    89,
      -1,    89,    -1,    85,    -1,    85,    56,    94,    -1,    94,
      -1,    87,    -1,    89,    -1,    91,    -1,    93,    -1,    86,
      46,    86,    -1,    86,    45,    86,    -1,    86,    47,    86,
      -1,    86,    48,    86,    -1,    86,    49,    86,    -1,    86,
      13,    86,    -1,    57,    86,    57,    -1,    45,    86,    -1,
      86,    51,    86,    -1,    58,    86,    59,    -1,    33,    86,
      59,    -1,    29,    86,    59,    -1,    30,    86,    59,    -1,
      31,    86,    59,    -1,    32,    86,    56,    86,    59,    -1,
      86,    12,    -1,     9,    -1,     3,    -1,    86,    -1,    58,
      88,    56,    88,    56,    88,    59,    -1,    58,    88,    56,
      88,    59,    -1,     4,    -1,    86,    -1,    54,    88,    56,
      88,    56,    88,    55,    -1,    54,    88,    56,    88,    55,
      -1,     5,    -1,    86,    -1,    54,    54,    88,    56,    88,
      56,    88,    56,    88,    55,    54,    88,    56,    88,    56,
      88,    56,    88,    55,    54,    88,    56,    88,    56,    88,
      56,    88,    55,    54,    88,    56,    88,    56,    88,    56,
      88,    55,    55,    -1,    24,    88,    59,    -1,    24,    88,
      56,    86,    59,    -1,    24,    88,    56,    90,    56,    92,
      59,    -1,    23,    92,    59,    -1,    25,    86,    59,    -1,
      26,    59,    -1,    26,    88,    59,    -1,    27,    88,    59,
      -1,    28,    90,    56,    86,    56,    92,    59,    -1,    28,
      90,    56,    86,    59,    -1,    28,    90,    59,    -1,    34,
      94,    59,    -1,     6,    -1,    86,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,   105,   105,   108,   109,   112,   113,   116,   120,   124,
     129,   133,   137,   141,   145,   148,   151,   152,   155,   156,
     159,   160,   163,   164,   167,   170,   171,   174,   175,   179,
     180,   181,   184,   185,   188,   189,   190,   193,   194,   195,
     196,   197,   201,   205,   209,   210,   211,   212,   213,   214,
     213,   223,   224,   227,   231,   238,   239,   240,   241,   244,
     247,   248,   251,   254,   255,   258,   259,   260,   261,   262,
     263,   264,   265,   266,   267,   268,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   280,   281,   284,   287,   291,
     295,   298,   301,   305,   309,   311,   314,   326,   330,   340,
     344,   348,   365,   366,   367,   368,   378,   388,   393,   394,
     397
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "ID", "PAREN_ID", "BRACKET_ID",
  "DBL_BRACKET_ID", "CURLY_ID", "ANGLE_ID", "NUM", "OPTS_STR", "SPECIAL",
  "TICK", "THEN", "DEF", "EMPTY_ANGLE", "DOTS", "LINE", "CURVE", "POLYGON",
  "REPEAT", "SWEEP", "PUT", "TRANSLATE", "ROTATE", "SCALE", "PROJECT",
  "PERSPECTIVE", "VIEW", "SQRT", "SIN", "COS", "ATAN2", "UNIT", "INVERSE",
  "GLOBAL", "SET", "PICTUREBOX", "FRAME", "CAMERA", "LANGUAGE", "PSTRICKS",
  "TIKZ", "LaTeX", "ConTeXt", "'-'", "'+'", "'*'", "'/'", "'.'", "NEG",
  "'^'", "'{'", "'}'", "'['", "']'", "','", "'|'", "'('", "')'", "$accept",
  "input", "global_decl_block", "global_decls", "global_decl",
  "output_language", "graphics_language", "comma_macro_package",
  "macro_package", "opt_baseline", "defs_and_decls", "rev_defs_and_decls",
  "def_or_decl", "def", "tagged_defs", "defable", "decl", "@1", "@2",
  "opt_star", "option_id_list", "options", "points", "rev_points",
  "transforms", "rev_transforms", "expr", "scalar", "scalar_expr", "point",
  "point_expr", "vector", "vector_expr", "transform", "transform_expr", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,    45,    43,    42,    47,    46,
     300,    94,   123,   125,    91,    93,    44,   124,    40,    41
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,    60,    61,    62,    62,    63,    63,    64,    64,    64,
      64,    64,    64,    64,    64,    65,    66,    66,    67,    67,
      68,    68,    69,    69,    70,    71,    71,    72,    72,    73,
      73,    73,    74,    74,    75,    75,    75,    76,    76,    76,
      76,    76,    76,    76,    76,    76,    76,    76,    77,    78,
      76,    79,    79,    80,    80,    81,    81,    81,    81,    82,
      83,    83,    84,    85,    85,    86,    86,    86,    86,    86,
      86,    86,    86,    86,    86,    86,    86,    86,    86,    86,
      86,    86,    86,    86,    86,    87,    87,    88,    89,    89,
      89,    90,    91,    91,    91,    92,    93,    93,    93,    93,
      93,    93,    93,    93,    93,    93,    93,    93,    93,    93,
      94
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     2,     4,     0,     2,     1,     2,     4,     4,
       2,     1,     2,     2,     1,     2,     1,     1,     2,     0,
       1,     1,     3,     0,     1,     2,     1,     1,     1,     3,
       3,     3,     4,     3,     1,     1,     1,     3,     3,     3,
       3,     9,     9,     7,     5,     3,     2,     1,     0,     0,
       5,     1,     0,     3,     1,     1,     1,     3,     0,     1,
       2,     1,     1,     3,     1,     1,     1,     1,     1,     3,
       3,     3,     3,     3,     3,     3,     2,     3,     3,     3,
       3,     3,     3,     5,     2,     1,     1,     1,     7,     5,
       1,     1,     7,     5,     1,     1,    38,     3,     5,     7,
       3,     3,     2,     3,     3,     7,     5,     3,     3,     1,
       1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned char yydefact[] =
{
       0,    47,    58,     0,    58,    58,    58,    58,     0,    58,
       0,    48,     0,     4,    24,    26,    27,     0,    28,    56,
      55,     0,    46,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     1,     0,     2,    25,     0,     0,    54,     0,
      90,     0,    45,    59,    61,    86,    94,   109,     0,    85,
      36,    31,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    29,    35,
      34,    65,    66,    67,    68,    37,    38,    39,    40,    87,
       0,     0,   110,     0,    49,     0,    33,    30,    57,     0,
       0,    60,    32,    95,     0,     0,     0,   102,     0,     0,
      91,     0,     0,     0,     0,     0,     0,     0,    76,     0,
       0,     0,    87,    84,     0,     0,     0,     0,     0,     0,
       0,     0,    52,     0,     0,     0,    23,    11,     0,     0,
       0,     6,    14,    53,     0,   100,     0,    97,   101,   103,
     104,     0,   107,    80,    81,    82,     0,    79,   108,     0,
       0,    75,    78,    74,    70,    69,    71,    72,    73,    77,
       0,    62,    64,    51,     0,    44,    50,     7,     0,     0,
      12,    10,    16,    17,    13,    19,     3,     5,     0,    91,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    15,     0,    89,    98,     0,     0,   106,    83,     0,
      93,     0,    43,    63,     0,     8,     9,    20,    21,    18,
       0,     0,     0,     0,     0,     0,    88,    99,   105,     0,
      92,    42,    41,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    96
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short int yydefgoto[] =
{
      -1,    12,    34,   130,   131,   174,   175,   191,   209,   169,
      13,    14,    15,    16,    17,    68,    69,    31,   124,   164,
      39,    22,    42,    43,   160,   161,    79,    71,    90,    72,
     101,    73,    94,    74,   162
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -94
static const short int yypact[] =
{
     520,   -94,     3,    21,     3,     3,     3,     3,   -35,     3,
     -26,   -94,    35,    11,   520,   -94,   -94,     6,   -94,   -94,
     -94,    49,    15,   255,    15,    15,    15,    15,    84,   -14,
      84,   520,   -94,    30,   -94,   -94,   323,   323,   -94,    -5,
     -94,    84,   -94,    15,   -94,   -94,   -94,   -94,   323,   -94,
     -94,   -94,    84,    84,    84,    38,    84,    84,    84,    84,
      84,    84,    84,    84,    84,   379,    84,    84,   -94,   -94,
     346,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   346,
      48,    84,   346,     5,   -94,   210,   -94,   -94,   -94,    83,
      63,   -94,   -94,   346,    32,   -41,   111,   -94,    61,    62,
     346,   -20,   133,   406,   426,   533,   433,    67,    -2,   379,
      72,   516,   448,   -94,    84,    84,    84,    84,    84,    84,
      84,    84,    79,   536,    77,   121,    78,   151,    84,    14,
     113,   -94,   -94,   -94,    84,   -94,    84,   -94,   -94,   -94,
     -94,    84,   -94,   -94,   -94,   -94,    84,   -94,   -94,    87,
      84,   -94,   -94,   346,   142,   142,    -2,    -2,    -2,    -2,
     112,   115,   -94,   -94,   116,   -94,   -94,   -94,    84,    15,
     -94,   -94,   -94,   -94,   -94,   118,   -94,   -94,   -11,   457,
     119,    88,   474,    84,     4,   536,    84,    84,   114,    15,
      36,   -94,    84,   -94,   -94,    84,    84,   -94,   -94,    29,
     -94,    84,   -94,   -94,   123,    16,   -94,   -94,   -94,   -94,
     108,   124,   127,    84,   122,   410,   -94,   -94,   -94,    43,
     -94,   -94,   -94,    84,   141,   134,    84,   143,    84,   147,
      84,   149,    84,   155,   152,    84,   156,    84,   163,    84,
     165,    84,   168,   171,    84,   172,    84,   173,    84,   175,
      84,   179,   188,   -94
};

/* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
     -94,   -94,   -94,   -94,    97,   -94,   -94,   -94,   -94,   -94,
     166,   -94,   219,   -76,   -94,   -25,     2,   -94,   -94,   -94,
     -94,    25,    51,   -94,    64,   -94,   178,   -94,   -28,   -21,
     117,   -94,   -93,   -94,   -23
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -23
static const short int yytable[] =
{
      80,    44,    18,    44,    44,    44,    44,    83,    19,   132,
     113,    86,    87,    20,    36,   136,    18,    28,   137,    40,
     -22,    37,    91,    92,    23,    95,    30,    98,    99,    24,
      25,    26,    27,    18,    29,    32,   141,   110,    81,   142,
     107,    45,    40,    46,    47,   192,    33,    49,   193,   120,
      88,    89,    38,   122,   132,   172,   173,    21,   123,   200,
     201,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    41,   -22,    75,    76,    77,    78,   207,
     208,   149,    85,    64,   200,   213,   133,    45,    40,    46,
      47,   135,    65,    49,   163,    66,    67,    97,   220,   223,
     113,   114,   211,   212,   121,   171,   178,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,   134,
     139,   140,   184,   113,   114,   165,   148,     3,   150,    64,
     166,   167,   168,   115,   116,   117,   118,   119,    65,   120,
     188,    66,    67,   183,   196,   113,   114,   197,   189,   125,
     126,   127,   128,   129,   113,   199,   115,   116,   117,   118,
     119,   170,   120,   203,   210,   185,   176,   216,   206,   205,
     138,   186,   187,   214,   190,   195,   215,   220,   115,   116,
     117,   118,   119,   217,   120,   219,   218,   202,   226,   117,
     118,   119,   143,   120,   222,   224,   225,    84,   227,   228,
     229,    70,   231,   230,   233,   232,   235,   236,    82,   238,
     234,   240,   237,   242,    70,    70,   245,   221,   247,   239,
     249,   241,   251,   243,     3,   244,    70,   177,   246,   248,
      93,   250,    96,    35,   252,   100,   102,   103,   104,   105,
     106,    82,   108,   253,   111,   112,   125,   126,   127,   128,
     129,   204,     0,   180,     0,     0,     0,     0,    45,    40,
      46,    47,     1,    48,    49,    50,     2,     0,     0,     0,
      51,     4,     5,     6,     7,     8,     9,    10,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
       0,     0,   153,   154,   155,   156,   157,   158,   159,    82,
      64,     0,     0,     0,     0,     0,    82,    11,     0,    65,
       0,     0,    66,    67,   179,     0,     0,     0,     0,   181,
       0,     0,     0,     0,   182,     0,    45,    40,    46,    47,
       1,     0,    49,    50,     2,     0,     0,     0,     0,     4,
       5,     6,     7,     8,     9,    10,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,   113,   114,
       0,     0,     0,     0,    82,    82,     0,     0,    64,     0,
       0,     0,     0,    93,    93,    11,     0,    65,     0,     0,
      66,    67,    45,    40,    46,    47,     0,     0,    49,     0,
       0,   115,   116,   117,   118,   119,     0,   120,     0,     0,
       0,     0,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    40,     0,     0,     1,   113,   114,
       0,     2,     0,     0,    64,     0,     4,     5,     6,     7,
       8,     9,    10,   109,     0,     0,    66,    67,   113,   114,
       0,     0,     0,     0,     0,   113,   114,     0,     0,     0,
       0,   115,   116,   117,   118,   119,     0,   120,     0,     0,
     113,   114,    11,     0,     0,   144,     0,     0,    41,   113,
     114,   115,   116,   117,   118,   119,     0,   120,   115,   116,
     117,   118,   119,     0,   120,   145,   113,   114,     0,     0,
       0,     0,   147,   115,   116,   117,   118,   119,     0,   120,
       0,     0,   115,   116,   117,   118,   119,   152,   120,     0,
       0,     0,     0,     0,     0,     0,   194,     0,     0,   115,
     116,   117,   118,   119,     0,   120,     0,     1,   113,   114,
       0,     2,     0,   198,     3,     0,     4,     5,     6,     7,
       8,     9,    10,     1,     0,   113,   114,     2,     0,     0,
       0,     0,     4,     5,     6,     7,     8,     9,    10,     0,
       0,   115,   116,   117,   118,   119,     0,   120,     0,     0,
       0,     0,    11,   151,     0,     0,     0,     0,   115,   116,
     117,   118,   119,     0,   120,     0,     0,     0,    11,   146
};

static const short int yycheck[] =
{
      28,    22,     0,    24,    25,    26,    27,    30,     5,    85,
      12,    36,    37,    10,     8,    56,    14,    52,    59,     4,
       4,    15,    43,    48,     3,    53,    52,    55,    56,     4,
       5,     6,     7,    31,     9,     0,    56,    65,    52,    59,
      63,     3,     4,     5,     6,    56,    35,     9,    59,    51,
      55,    56,     3,    81,   130,    41,    42,    54,    53,    55,
      56,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    58,    58,    24,    25,    26,    27,    43,
      44,   109,    52,    45,    55,    56,     3,     3,     4,     5,
       6,    59,    54,     9,    15,    57,    58,    59,    55,    56,
      12,    13,   195,   196,    56,   128,   134,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    56,
      59,    59,   150,    12,    13,   123,    59,    14,    56,    45,
      53,    10,    54,    45,    46,    47,    48,    49,    54,    51,
     168,    57,    58,    56,    56,    12,    13,    59,   169,    36,
      37,    38,    39,    40,    12,   183,    45,    46,    47,    48,
      49,    10,    51,   186,   192,    53,    53,    59,   189,    55,
      59,    56,    56,   201,    56,    56,    53,    55,    45,    46,
      47,    48,    49,    59,    51,   213,    59,   185,    54,    47,
      48,    49,    59,    51,   215,   223,    55,    31,   226,    56,
     228,    23,   230,    56,   232,    56,    54,   235,    30,   237,
      55,   239,    56,   241,    36,    37,   244,   215,   246,    56,
     248,    56,   250,    55,    14,    54,    48,   130,    56,    56,
      52,    56,    54,    14,    55,    57,    58,    59,    60,    61,
      62,    63,    64,    55,    66,    67,    36,    37,    38,    39,
      40,   187,    -1,   136,    -1,    -1,    -1,    -1,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    -1,    -1,    -1,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      -1,    -1,   114,   115,   116,   117,   118,   119,   120,   121,
      45,    -1,    -1,    -1,    -1,    -1,   128,    52,    -1,    54,
      -1,    -1,    57,    58,   136,    -1,    -1,    -1,    -1,   141,
      -1,    -1,    -1,    -1,   146,    -1,     3,     4,     5,     6,
       7,    -1,     9,    10,    11,    -1,    -1,    -1,    -1,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    12,    13,
      -1,    -1,    -1,    -1,   186,   187,    -1,    -1,    45,    -1,
      -1,    -1,    -1,   195,   196,    52,    -1,    54,    -1,    -1,
      57,    58,     3,     4,     5,     6,    -1,    -1,     9,    -1,
      -1,    45,    46,    47,    48,    49,    -1,    51,    -1,    -1,
      -1,    -1,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,     4,    -1,    -1,     7,    12,    13,
      -1,    11,    -1,    -1,    45,    -1,    16,    17,    18,    19,
      20,    21,    22,    54,    -1,    -1,    57,    58,    12,    13,
      -1,    -1,    -1,    -1,    -1,    12,    13,    -1,    -1,    -1,
      -1,    45,    46,    47,    48,    49,    -1,    51,    -1,    -1,
      12,    13,    52,    -1,    -1,    59,    -1,    -1,    58,    12,
      13,    45,    46,    47,    48,    49,    -1,    51,    45,    46,
      47,    48,    49,    -1,    51,    59,    12,    13,    -1,    -1,
      -1,    -1,    59,    45,    46,    47,    48,    49,    -1,    51,
      -1,    -1,    45,    46,    47,    48,    49,    59,    51,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    45,
      46,    47,    48,    49,    -1,    51,    -1,     7,    12,    13,
      -1,    11,    -1,    59,    14,    -1,    16,    17,    18,    19,
      20,    21,    22,     7,    -1,    12,    13,    11,    -1,    -1,
      -1,    -1,    16,    17,    18,    19,    20,    21,    22,    -1,
      -1,    45,    46,    47,    48,    49,    -1,    51,    -1,    -1,
      -1,    -1,    52,    57,    -1,    -1,    -1,    -1,    45,    46,
      47,    48,    49,    -1,    51,    -1,    -1,    -1,    52,    56
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,     7,    11,    14,    16,    17,    18,    19,    20,    21,
      22,    52,    61,    70,    71,    72,    73,    74,    76,     5,
      10,    54,    81,     3,    81,    81,    81,    81,    52,    81,
      52,    77,     0,    35,    62,    72,     8,    15,     3,    80,
       4,    58,    82,    83,    89,     3,     5,     6,     8,     9,
      10,    15,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    45,    54,    57,    58,    75,    76,
      86,    87,    89,    91,    93,    82,    82,    82,    82,    86,
      88,    52,    86,    94,    70,    52,    75,    75,    55,    56,
      88,    89,    75,    86,    92,    88,    86,    59,    88,    88,
      86,    90,    86,    86,    86,    86,    86,    94,    86,    54,
      88,    86,    86,    12,    13,    45,    46,    47,    48,    49,
      51,    56,    88,    53,    78,    36,    37,    38,    39,    40,
      63,    64,    73,     3,    56,    59,    56,    59,    59,    59,
      59,    56,    59,    59,    59,    59,    56,    59,    59,    88,
      56,    57,    59,    86,    86,    86,    86,    86,    86,    86,
      84,    85,    94,    15,    79,    76,    53,    10,    54,    69,
      10,    94,    41,    42,    65,    66,    53,    64,    88,    86,
      90,    86,    86,    56,    88,    53,    56,    56,    88,    89,
      56,    67,    56,    59,    59,    56,    56,    59,    59,    88,
      55,    56,    76,    94,    84,    55,    89,    43,    44,    68,
      88,    92,    92,    56,    88,    53,    59,    59,    59,    88,
      55,    76,    89,    56,    88,    55,    54,    88,    56,    88,
      56,    88,    56,    88,    55,    54,    88,    56,    88,    56,
      88,    56,    88,    55,    54,    88,    56,    88,    56,    88,
      56,    88,    55,    55
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (0)


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (N)								\
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (0)
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
              (Loc).first_line, (Loc).first_column,	\
              (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr,					\
                  Type, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname[yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);


# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()
    ;
#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a look-ahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to look-ahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 105 "sketch.y"
    { objects = (yyvsp[-1].obj); }
    break;

  case 7:
#line 117 "sketch.y"
    { 
                            set_global_env_opts(global_env, (yyvsp[0].str), line);
                          }
    break;

  case 8:
#line 121 "sketch.y"
    { 
                            set_global_baseline(global_env, (yyvsp[-1].flt), line);
                          }
    break;

  case 9:
#line 125 "sketch.y"
    { 
                            set_global_baseline(global_env, (yyvsp[-2].flt), line);
                            set_global_env_extent(global_env, (yyvsp[-1].pt), (yyvsp[0].pt), line);
                          }
    break;

  case 10:
#line 130 "sketch.y"
    {
                            set_global_env_camera(global_env, (yyvsp[0].xf), line);
                          }
    break;

  case 11:
#line 134 "sketch.y"
    { 
                            set_global_env_frame(global_env, NULL, line);
                          }
    break;

  case 12:
#line 138 "sketch.y"
    { 
                            set_global_env_frame(global_env, (yyvsp[0].str), line);
                          }
    break;

  case 13:
#line 142 "sketch.y"
    {
                            set_global_output_language(global_env, (yyvsp[0].index), line);
		          }
    break;

  case 15:
#line 148 "sketch.y"
    { (yyval.index) = (yyvsp[-1].index) | (yyvsp[0].index); }
    break;

  case 16:
#line 151 "sketch.y"
    { (yyval.index) = GEOL_PSTRICKS; }
    break;

  case 17:
#line 152 "sketch.y"
    { (yyval.index) = GEOL_TIKZ; }
    break;

  case 18:
#line 155 "sketch.y"
    { (yyval.index) = (yyvsp[0].index); }
    break;

  case 19:
#line 156 "sketch.y"
    { (yyval.index) = GEOL_LATEX; }
    break;

  case 20:
#line 159 "sketch.y"
    { (yyval.index) = GEOL_LATEX; }
    break;

  case 21:
#line 160 "sketch.y"
    { (yyval.index) = GEOL_CONTEXT; }
    break;

  case 22:
#line 163 "sketch.y"
    { (yyval.flt) = (yyvsp[-1].flt); }
    break;

  case 23:
#line 164 "sketch.y"
    { (yyval.flt) = NO_BASELINE; }
    break;

  case 24:
#line 167 "sketch.y"
    { (yyval.obj) = sibling_reverse((yyvsp[0].obj)); }
    break;

  case 25:
#line 170 "sketch.y"
    { (yyval.obj) = cat_objects((yyvsp[0].obj), (yyvsp[-1].obj)); }
    break;

  case 26:
#line 171 "sketch.y"
    { (yyval.obj) = (yyvsp[0].obj); }
    break;

  case 27:
#line 174 "sketch.y"
    { (yyval.obj) = NULL; }
    break;

  case 28:
#line 175 "sketch.y"
    { (yyval.obj) = (yyvsp[0].obj); }
    break;

  case 29:
#line 179 "sketch.y"
    { new_symbol(sym_tab, (yyvsp[-1].name), 0, (yyvsp[0].obj), line); }
    break;

  case 30:
#line 180 "sketch.y"
    { new_symbol(sym_tab, (yyvsp[-2].name), 0, (yyvsp[0].obj), line); }
    break;

  case 31:
#line 181 "sketch.y"
    { new_symbol(sym_tab, (yyvsp[-1].name), 0, new_tag_def(), line); }
    break;

  case 32:
#line 184 "sketch.y"
    { strcpy((yyval.name), new_symbol(sym_tab, (yyvsp[-2].name), (yyvsp[-1].name), (yyvsp[0].obj), line) ? "" : (yyvsp[-2].name)); }
    break;

  case 33:
#line 185 "sketch.y"
    { strcpy((yyval.name), new_symbol(sym_tab, (yyvsp[-2].name), (yyvsp[-1].name), (yyvsp[0].obj), line) ? "" : (yyvsp[-2].name)); }
    break;

  case 34:
#line 188 "sketch.y"
    { (yyval.obj) = object_from_expr(&(yyvsp[0].exv)); }
    break;

  case 35:
#line 189 "sketch.y"
    { (yyval.obj) = (yyvsp[0].obj); }
    break;

  case 36:
#line 190 "sketch.y"
    { (yyval.obj) = new_opts_def((yyvsp[0].str), line); }
    break;

  case 37:
#line 193 "sketch.y"
    { (yyval.obj) = new_dots((yyvsp[-1].opts), (yyvsp[0].obj)); }
    break;

  case 38:
#line 194 "sketch.y"
    { (yyval.obj) = new_line((yyvsp[-1].opts), (yyvsp[0].obj)); }
    break;

  case 39:
#line 195 "sketch.y"
    { (yyval.obj) = new_curve((yyvsp[-1].opts), (yyvsp[0].obj)); }
    break;

  case 40:
#line 196 "sketch.y"
    { (yyval.obj) = new_polygon((yyvsp[-1].opts), (yyvsp[0].obj)); }
    break;

  case 41:
#line 198 "sketch.y"
    { 
						                (yyval.obj) = new_sweep((yyvsp[-7].opts), (yyvsp[-5].flt), (yyvsp[-4].bool), (yyvsp[-2].obj), new_point_def((yyvsp[0].pt)));
						              }
    break;

  case 42:
#line 202 "sketch.y"
    {
						                (yyval.obj) = new_sweep((yyvsp[-7].opts), (yyvsp[-5].flt), (yyvsp[-4].bool), (yyvsp[-2].obj), (yyvsp[0].obj));
						              }
    break;

  case 43:
#line 206 "sketch.y"
    {
						                (yyval.obj) = new_repeat((yyvsp[-4].flt), (yyvsp[-2].obj), (yyvsp[0].obj));
						              }
    break;

  case 44:
#line 209 "sketch.y"
    { (yyval.obj) = new_compound((yyvsp[-2].xf), (yyvsp[0].obj)); }
    break;

  case 45:
#line 210 "sketch.y"
    { (yyval.obj) = new_special((yyvsp[-2].str), (yyvsp[-1].opts), (yyvsp[0].obj), line); }
    break;

  case 46:
#line 211 "sketch.y"
    { (yyval.obj) = new_special((yyvsp[-1].str), (yyvsp[0].opts), new_point_def(origin_3d), line); }
    break;

  case 47:
#line 212 "sketch.y"
    { look_up_drawable(sym_tab, &(yyval.obj), line, (yyvsp[0].name)); }
    break;

  case 48:
#line 213 "sketch.y"
    { sym_tab = new_scope(sym_tab); }
    break;

  case 49:
#line 214 "sketch.y"
    { sym_tab = old_scope(sym_tab); }
    break;

  case 50:
#line 216 "sketch.y"
    {
                            if ((yyvsp[-2].obj) == NULL)
                              err(line, "no drawables in compound declaration");
                            (yyval.obj) = (yyvsp[-2].obj);
                          }
    break;

  case 51:
#line 223 "sketch.y"
    { (yyval.bool) = 1; }
    break;

  case 52:
#line 224 "sketch.y"
    { (yyval.bool) = 0; }
    break;

  case 53:
#line 228 "sketch.y"
    { 
                            (yyval.opts) = look_up_and_append_to_opts(sym_tab, &(yyvsp[-2].opts), line, (yyvsp[0].name));
			  }
    break;

  case 54:
#line 232 "sketch.y"
    { 
                            (yyval.opts) = NULL;
                            (yyval.opts) = look_up_and_append_to_opts(sym_tab, &(yyval.opts), line, (yyvsp[0].name));
			  }
    break;

  case 55:
#line 238 "sketch.y"
    { (yyval.opts) = new_opts((yyvsp[0].str), line); }
    break;

  case 56:
#line 239 "sketch.y"
    { look_up_opts(sym_tab, &(yyval.opts), line, (yyvsp[0].name)); }
    break;

  case 57:
#line 240 "sketch.y"
    { (yyval.opts) = (yyvsp[-1].opts); }
    break;

  case 58:
#line 241 "sketch.y"
    { (yyval.opts) = NULL; }
    break;

  case 59:
#line 244 "sketch.y"
    { (yyval.obj) = sibling_reverse((yyvsp[0].obj)); }
    break;

  case 60:
#line 247 "sketch.y"
    { (yyval.obj) = cat_objects(new_point_def((yyvsp[0].pt)), (yyvsp[-1].obj)); }
    break;

  case 61:
#line 248 "sketch.y"
    { (yyval.obj) = new_point_def((yyvsp[0].pt)); }
    break;

  case 62:
#line 251 "sketch.y"
    { (yyval.obj) = sibling_reverse((yyvsp[0].obj)); }
    break;

  case 63:
#line 254 "sketch.y"
    { (yyval.obj) = cat_objects(new_transform_def((yyvsp[0].xf)), (yyvsp[-2].obj)); }
    break;

  case 64:
#line 255 "sketch.y"
    { (yyval.obj) = new_transform_def((yyvsp[0].xf)); }
    break;

  case 65:
#line 258 "sketch.y"
    { set_float(&(yyval.exv), (yyvsp[0].flt)); }
    break;

  case 66:
#line 259 "sketch.y"
    { set_point(&(yyval.exv), (yyvsp[0].pt)); }
    break;

  case 67:
#line 260 "sketch.y"
    { set_vector(&(yyval.exv), (yyvsp[0].vec)); }
    break;

  case 68:
#line 261 "sketch.y"
    { set_transform(&(yyval.exv), (yyvsp[0].xf)); }
    break;

  case 69:
#line 262 "sketch.y"
    { do_add(&(yyval.exv), &(yyvsp[-2].exv), &(yyvsp[0].exv), line); }
    break;

  case 70:
#line 263 "sketch.y"
    { do_sub(&(yyval.exv), &(yyvsp[-2].exv), &(yyvsp[0].exv), line); }
    break;

  case 71:
#line 264 "sketch.y"
    { do_mul(&(yyval.exv), &(yyvsp[-2].exv), &(yyvsp[0].exv), line); }
    break;

  case 72:
#line 265 "sketch.y"
    { do_dvd(&(yyval.exv), &(yyvsp[-2].exv), &(yyvsp[0].exv), line); }
    break;

  case 73:
#line 266 "sketch.y"
    { do_dot(&(yyval.exv), &(yyvsp[-2].exv), &(yyvsp[0].exv), line); }
    break;

  case 74:
#line 267 "sketch.y"
    { do_thn(&(yyval.exv), &(yyvsp[-2].exv), &(yyvsp[0].exv), line); }
    break;

  case 75:
#line 268 "sketch.y"
    { do_mag(&(yyval.exv), &(yyvsp[-1].exv), line); }
    break;

  case 76:
#line 269 "sketch.y"
    { do_neg(&(yyval.exv), &(yyvsp[0].exv), line); }
    break;

  case 77:
#line 270 "sketch.y"
    { do_pwr(&(yyval.exv), &(yyvsp[-2].exv), &(yyvsp[0].exv), line); }
    break;

  case 78:
#line 271 "sketch.y"
    { (yyval.exv) = (yyvsp[-1].exv); }
    break;

  case 79:
#line 272 "sketch.y"
    { do_unit(&(yyval.exv), &(yyvsp[-1].exv), line); }
    break;

  case 80:
#line 273 "sketch.y"
    { do_sqrt(&(yyval.exv), &(yyvsp[-1].exv), line); }
    break;

  case 81:
#line 274 "sketch.y"
    { do_sin(&(yyval.exv), &(yyvsp[-1].exv), line); }
    break;

  case 82:
#line 275 "sketch.y"
    { do_cos(&(yyval.exv), &(yyvsp[-1].exv), line); }
    break;

  case 83:
#line 276 "sketch.y"
    { do_atan2(&(yyval.exv), &(yyvsp[-3].exv), &(yyvsp[-1].exv), line); }
    break;

  case 84:
#line 277 "sketch.y"
    { do_index(&(yyval.exv), &(yyvsp[-1].exv), (yyvsp[0].index), line); }
    break;

  case 85:
#line 280 "sketch.y"
    { (yyval.flt) = (yyvsp[0].flt); }
    break;

  case 86:
#line 281 "sketch.y"
    { look_up_scalar(sym_tab, &(yyval.flt), line, (yyvsp[0].name)); }
    break;

  case 87:
#line 284 "sketch.y"
    { coerce_to_float(&(yyvsp[0].exv), &(yyval.flt), line); }
    break;

  case 88:
#line 288 "sketch.y"
    { 
                            (yyval.pt)[X] = (yyvsp[-5].flt); (yyval.pt)[Y] = (yyvsp[-3].flt); (yyval.pt)[Z] = (yyvsp[-1].flt);
                          }
    break;

  case 89:
#line 292 "sketch.y"
    { 
                            (yyval.pt)[X] = (yyvsp[-3].flt); (yyval.pt)[Y] = (yyvsp[-1].flt); (yyval.pt)[Z] = 0;
                          }
    break;

  case 90:
#line 295 "sketch.y"
    { look_up_point(sym_tab, (yyval.pt), line, (yyvsp[0].name)); }
    break;

  case 91:
#line 298 "sketch.y"
    { coerce_to_point(&(yyvsp[0].exv), (yyval.pt), line); }
    break;

  case 92:
#line 302 "sketch.y"
    { 
                            (yyval.vec)[X] = (yyvsp[-5].flt); (yyval.vec)[Y] = (yyvsp[-3].flt); (yyval.vec)[Z] = (yyvsp[-1].flt);
                          }
    break;

  case 93:
#line 306 "sketch.y"
    { 
                            (yyval.vec)[X] = (yyvsp[-3].flt); (yyval.vec)[Y] = (yyvsp[-1].flt); (yyval.vec)[Z] = 0;
                          }
    break;

  case 94:
#line 309 "sketch.y"
    { look_up_vector(sym_tab, (yyval.vec), line, (yyvsp[0].name)); }
    break;

  case 95:
#line 311 "sketch.y"
    { coerce_to_vector(&(yyvsp[0].exv), (yyval.vec), line); }
    break;

  case 96:
#line 320 "sketch.y"
    { // transform is column major while elements are row major
                            (yyval.xf)[0] = (yyvsp[-35].flt);  (yyval.xf)[4] = (yyvsp[-33].flt);  (yyval.xf)[8]  = (yyvsp[-31].flt);  (yyval.xf)[12] = (yyvsp[-29].flt);
                            (yyval.xf)[1] = (yyvsp[-26].flt); (yyval.xf)[5] = (yyvsp[-24].flt); (yyval.xf)[9]  = (yyvsp[-22].flt); (yyval.xf)[13] = (yyvsp[-20].flt);
                            (yyval.xf)[2] = (yyvsp[-17].flt); (yyval.xf)[6] = (yyvsp[-15].flt); (yyval.xf)[10] = (yyvsp[-13].flt); (yyval.xf)[14] = (yyvsp[-11].flt);
                            (yyval.xf)[3] = (yyvsp[-8].flt); (yyval.xf)[7] = (yyvsp[-6].flt); (yyval.xf)[11] = (yyvsp[-4].flt); (yyval.xf)[15] = (yyvsp[-2].flt);
                          }
    break;

  case 97:
#line 327 "sketch.y"
    {
                            set_angle_axis_rot_about_point((yyval.xf), (yyvsp[-1].flt) * (PI/180), 0, 0);
                          }
    break;

  case 98:
#line 331 "sketch.y"
    {
                            if (EXPR_TYPE_IS(&(yyvsp[-1].exv), E_POINT))
                              set_angle_axis_rot_about_point((yyval.xf), (yyvsp[-3].flt) * (PI/180), (yyvsp[-1].exv).val.pt, 0);
                            else if (EXPR_TYPE_IS(&(yyvsp[-1].exv), E_VECTOR))
                              set_angle_axis_rot_about_point((yyval.xf), (yyvsp[-3].flt) * (PI/180), 0, (yyvsp[-1].exv).val.vec);
                            else
                              err(line, "expected point or vector rotation parameter, and it's a %s",
                                expr_val_type_str[(yyvsp[-1].exv).tag]);
                          }
    break;

  case 99:
#line 341 "sketch.y"
    {
                            set_angle_axis_rot_about_point((yyval.xf), (yyvsp[-5].flt) * (PI/180), (yyvsp[-3].pt), (yyvsp[-1].vec));
                          }
    break;

  case 100:
#line 345 "sketch.y"
    {
                            set_translation((yyval.xf), (yyvsp[-1].vec)[X], (yyvsp[-1].vec)[Y], (yyvsp[-1].vec)[Z]);
                          }
    break;

  case 101:
#line 349 "sketch.y"
    {
                            if ((yyvsp[-1].exv).tag == E_FLOAT) {
                              FLOAT s = (yyvsp[-1].exv).val.flt;
                              set_scale((yyval.xf), s, s, s);
                            }
                            else if ((yyvsp[-1].exv).tag == E_VECTOR) {
                              VECTOR v = (yyvsp[-1].exv).val.vec;
                              set_scale((yyval.xf), v[X], v[Y], v[Z]);
                            }
                            else {
                              err(line, 
                                "expected scalar or vector scale parameter, and it's a %s",
                                expr_val_type_str[(yyvsp[-1].exv).tag]);
                              set_ident((yyval.xf));
                            }
                          }
    break;

  case 102:
#line 365 "sketch.y"
    { set_parallel_projection((yyval.xf)); }
    break;

  case 103:
#line 366 "sketch.y"
    { set_perspective_projection((yyval.xf), (yyvsp[-1].flt)); }
    break;

  case 104:
#line 367 "sketch.y"
    { set_perspective_transform((yyval.xf), (yyvsp[-1].flt)); }
    break;

  case 105:
#line 369 "sketch.y"
    {
                            if ((yyvsp[-3].exv).tag == E_VECTOR)
                              set_view_transform((yyval.xf), (yyvsp[-5].pt), (yyvsp[-3].exv).val.vec, (yyvsp[-1].vec));
                            else if ((yyvsp[-3].exv).tag == E_POINT)
                              set_view_transform_with_look_at((yyval.xf), (yyvsp[-5].pt), (yyvsp[-3].exv).val.pt, (yyvsp[-1].vec));
                            else
                              err(line, "expected point or vector view parameter, and it's a %s",
                                expr_val_type_str[(yyvsp[-3].exv).tag]);
                          }
    break;

  case 106:
#line 379 "sketch.y"
    {
                            if ((yyvsp[-1].exv).tag == E_VECTOR)
                              set_view_transform((yyval.xf), (yyvsp[-3].pt), (yyvsp[-1].exv).val.vec, NULL);
                            else if ((yyvsp[-1].exv).tag == E_POINT)
                              set_view_transform_with_look_at((yyval.xf), (yyvsp[-3].pt), (yyvsp[-1].exv).val.pt, NULL);
                            else
                              err(line, "expected point or vector view parameter, and it's a %s",
                                expr_val_type_str[(yyvsp[-1].exv).tag]);
                          }
    break;

  case 107:
#line 389 "sketch.y"
    {
                            set_view_transform((yyval.xf), (yyvsp[-1].pt), NULL, NULL);
                          }
    break;

  case 108:
#line 393 "sketch.y"
    { do_inverse((yyval.xf), (yyvsp[-1].xf), line); }
    break;

  case 109:
#line 394 "sketch.y"
    { look_up_transform(sym_tab, (yyval.xf), line, (yyvsp[0].name)); }
    break;

  case 110:
#line 397 "sketch.y"
    { coerce_to_transform(&(yyvsp[0].exv), (yyval.xf), line); }
    break;


      default: break;
    }

/* Line 1126 of yacc.c.  */
#line 2126 "y.tab.c"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  int yytype = YYTRANSLATE (yychar);
	  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
	  YYSIZE_T yysize = yysize0;
	  YYSIZE_T yysize1;
	  int yysize_overflow = 0;
	  char *yymsg = 0;
#	  define YYERROR_VERBOSE_ARGS_MAXIMUM 5
	  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
	  int yyx;

#if 0
	  /* This is so xgettext sees the translatable formats that are
	     constructed on the fly.  */
	  YY_("syntax error, unexpected %s");
	  YY_("syntax error, unexpected %s, expecting %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
#endif
	  char *yyfmt;
	  char const *yyf;
	  static char const yyunexpected[] = "syntax error, unexpected %s";
	  static char const yyexpecting[] = ", expecting %s";
	  static char const yyor[] = " or %s";
	  char yyformat[sizeof yyunexpected
			+ sizeof yyexpecting - 1
			+ ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
			   * (sizeof yyor - 1))];
	  char const *yyprefix = yyexpecting;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 1;

	  yyarg[0] = yytname[yytype];
	  yyfmt = yystpcpy (yyformat, yyunexpected);

	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
		  {
		    yycount = 1;
		    yysize = yysize0;
		    yyformat[sizeof yyunexpected - 1] = '\0';
		    break;
		  }
		yyarg[yycount++] = yytname[yyx];
		yysize1 = yysize + yytnamerr (0, yytname[yyx]);
		yysize_overflow |= yysize1 < yysize;
		yysize = yysize1;
		yyfmt = yystpcpy (yyfmt, yyprefix);
		yyprefix = yyor;
	      }

	  yyf = YY_(yyformat);
	  yysize1 = yysize + yystrlen (yyf);
	  yysize_overflow |= yysize1 < yysize;
	  yysize = yysize1;

	  if (!yysize_overflow && yysize <= YYSTACK_ALLOC_MAXIMUM)
	    yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg)
	    {
	      /* Avoid sprintf, as that infringes on the user's name space.
		 Don't have undefined behavior even if the translation
		 produced a string with the wrong number of "%s"s.  */
	      char *yyp = yymsg;
	      int yyi = 0;
	      while ((*yyp = *yyf))
		{
		  if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		    {
		      yyp += yytnamerr (yyp, yyarg[yyi++]);
		      yyf += 2;
		    }
		  else
		    {
		      yyp++;
		      yyf++;
		    }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    {
	      yyerror (YY_("syntax error"));
	      goto yyexhaustedlab;
	    }
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror (YY_("syntax error"));
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
        }
      else
	{
	  yydestruct ("Error: discarding", yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (0)
     goto yyerrorlab;

yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping", yystos[yystate], yyvsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token. */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK;
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 400 "sketch.y"


int parse(SYMBOL_TABLE *st)
{
  int ret;

	objects = NULL;
  sym_tab = st;
  ret = yyparse();

  // should set sym_tab back to NULL
  sym_tab = old_scope(sym_tab);
  if (sym_tab)
    die(no_line, "zombie symbol table");

  return ret;
}

OBJECT *parsed_objects(void)
{
	return objects;
}

