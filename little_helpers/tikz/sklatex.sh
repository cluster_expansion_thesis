#!/bin/bash -eu

pdf_viewer="xpdf -z page  "

show_pdf=""
#Default scaling
scaling="0.3"
#Parse options
while getopts "phs:" optionName
do
  case "${optionName}" in
    p) show_pdf="yes";;
    s) scaling=${OPTARG} ;;
    h) echo -e "Usage: ${0} [-p] SK_FILE \n "; 
       exit;;
  esac
done

#Switch to not processed arguments
shift $(($OPTIND - 1))


if [  ${#}  -eq 0 ] 
then
  ${0} -h
  exit
fi

sk=${1}

trunk=$(basename ${sk} .sk)
pdf=${trunk}.pdf
tex=${trunk}.tex

#Change language to tikz if not set
tmp_sk=$(mktemp)
if [ ! "$(grep "language\s*tikz" ${sk})" ]
then 
  cp ${sk} ${tmp_sk}
  echo "global {language tikz}" >> ${tmp_sk}
  sk=${tmp_sk}
fi

#run sketch
sketch ${sk} -Te -o ${tex}
rm -f ${tmp_sk}

#Adapt
adapt_sk.sh -s ${scaling} ${tex}

#TeX
pdflatex ${tex} >/dev/null

if [ "${show_pdf}" = "yes" ]
then
  ${pdf_viewer} ${pdf} &
fi

