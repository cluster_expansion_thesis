#!/bin/bash -eu


#Parse options
while getopts "h" optionName
do
  case "${optionName}" in
    h) echo -e "Usage: ${0} SK_FILE \n "; 
       exit;;
  esac
done

#Switch to not processed arguments
shift $(($OPTIND - 1))


if [  ${#}  -eq 0 ] 
then
  ${0} -h
  exit
fi

sk=${1}

trunk=$(basename ${sk} .sk)
tikz=${trunk}.tikz

#Change language to tikz if not set
if [ ! $(grep "language\s*tikz" ${sk}) ]
then 
  tmp_sk=$(mktemp)
  cp ${sk} ${tmp_sk}
  echo "global {language tikz}" >> ${tmp_sk}
  sk=${tmp_sk}
fi

#run sketch
sketch ${sk} -o ${tikz}
rm -f ${tmp_sk}

#Adapt
adapt_sk.sh ${tikz}
